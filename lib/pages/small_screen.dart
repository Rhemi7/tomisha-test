import 'package:flutter/material.dart';
import 'package:tomisha_test/pages/widget/app_button.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/first_tab_view.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/second_tab_view.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/tab_bar_widgets.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/third_tab_view.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/top_half_widget.dart';
import '../app_bar.dart';
import '../constants/color.dart';
import '../helpers/enum.dart';
import '../helpers/margin.dart';

class SmallScreen extends StatefulWidget {
  const SmallScreen({Key? key}) : super(key: key);

  @override
  State<SmallScreen> createState() => _SmallScreenState();
}

class _SmallScreenState extends State<SmallScreen> {
  SelectionEnum selected = SelectionEnum.first;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  const CustomAppBar(),
      body: Container(
        color: headerBg,
        child: ListView(
          children: [
            const TopHalfWidget(),
            Container(
              decoration: BoxDecoration(
                  color: appBarBg,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 5,
                      offset: const Offset(
                          0, 3), // changes the position of the shadow
                    ),
                  ],
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: Column(
                children: [
                  const YMargin(20),
                  const AppButton(),
                  const YMargin(50),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          border: Border.all(color: tabBorder)),
                      child: Row(
                        children: [
                          CornerTab(
                            title: "Arbeitnehmer",
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(14),
                                bottomLeft: Radius.circular(14)),
                            textColor: selected == SelectionEnum.first
                                ? Colors.white
                                : tabTextInactive,
                            bgColor: selected == SelectionEnum.first
                                ? tabActive
                                : Colors.white,
                            onTap: () {
                              setState(() {
                                selected = SelectionEnum.first;
                              });
                            },
                          ),
                          const AppVertDivider(),
                          CenterTab(
                            title: "Arbeitgeber",
                            textColor: selected == SelectionEnum.second
                                ? Colors.white
                                : tabTextInactive,
                            bgColor: selected == SelectionEnum.second
                                ? tabActive
                                : Colors.white,
                            onTap: () {
                              setState(() {
                                selected = SelectionEnum.second;
                              });
                            },
                          ),
                          const AppVertDivider(),
                          CornerTab(
                            title: "Temporärbüro",
                            borderRadius: const BorderRadius.only(
                                topRight: Radius.circular(14),
                                bottomRight: Radius.circular(14)),
                            textColor: selected == SelectionEnum.third
                                ? Colors.white
                                : tabTextInactive,
                            bgColor: selected == SelectionEnum.third
                                ? tabActive
                                : Colors.white,
                            onTap: () {
                              setState(() {
                                selected = SelectionEnum.third;
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  selected == SelectionEnum.first
                      ? const FirstTabView()
                      : selected == SelectionEnum.second
                      ? const SecondTabView()
                      : const ThirdTabView()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


