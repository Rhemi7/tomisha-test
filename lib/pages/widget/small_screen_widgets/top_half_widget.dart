import 'package:flutter/material.dart';

import '../../../constants/color.dart';
import '../../../helpers/margin.dart';

class TopHalfWidget extends StatelessWidget {
  const TopHalfWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 552,
      color: headerBg,
      child: Column(
        children: [
          const YMargin(20),
          const Text(
            "Deine job\nwebsite",
            style: TextStyle(fontSize: 30),
            textAlign: TextAlign.center,
          ),
          const YMargin(50),
          Container(
            height: 410,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/handshake.png"),
                    scale: 2,
                    fit: BoxFit.fitWidth)),
          )
        ],
      ),
    );
  }
}
