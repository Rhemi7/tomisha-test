import 'package:flutter/material.dart';

import '../../../constants/color.dart';

class AppVertDivider extends StatelessWidget {
  const AppVertDivider({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 1, // Width of the vertical divider
      height: double.infinity, // Make it as tall as the parent container
      color: tabBorder,
    );
  }
}

class CenterTab extends StatelessWidget {
  final String title;
  final Color bgColor;
  final Color textColor;
  final Function() onTap;
  const CenterTab({
    Key? key,
    required this.title,
    required this.bgColor,
    required this.textColor,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          color: bgColor,
          child: Center(
            child: Text(
              title,
              style: TextStyle(color: textColor),
            ),
          ),
        ),
      ),
    );
  }
}

class CornerTab extends StatelessWidget {
  final BorderRadius borderRadius;
  final String title;
  final Color textColor;
  final Color bgColor;
  final Function() onTap;
  const CornerTab({
    Key? key,
    required this.borderRadius,
    required this.title,
    required this.textColor,
    required this.bgColor,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: borderRadius,
            color: bgColor,
          ),
          child: Center(
            child: Text(
              title,
              style: TextStyle(color: textColor),
            ),
          ),
        ),
      ),
    );
  }
}