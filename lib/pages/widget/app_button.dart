import 'package:flutter/material.dart';

import '../../constants/color.dart';

class AppButton extends StatelessWidget {
  const AppButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Container(
        height: 50,
        decoration: BoxDecoration(
            gradient: const LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [tabTextInactive, buttonGradient]),
            borderRadius: BorderRadius.circular(15)),
        child: const Center(
            child: Text(
              "Kostenlos Registrieren",
              style: TextStyle(color: Colors.white),
            )),
      ),
    );
  }
}
