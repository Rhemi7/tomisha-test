import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/small_screen_shape.dart';

import '../../../constants/color.dart';
import '../../../helpers/margin.dart';
class FirstTabView extends StatelessWidget {
  const FirstTabView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const YMargin(50),
        const Text(
          "Drei einfache Schritte\nzu deinem neuen Job",
          style: TextStyle(color: subHeader, fontSize: 22),
        ),
        const YMargin(50),
        SizedBox(
          height: 320,
          width: double.infinity,
          child: Stack(
            children: [
              Container(
                height: 200,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        "assets/images/profile.png"),
                  ),
                ),
              ),
              Positioned(
                top: 150,
                left: 0,
                right: 0,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "1",
                      style: TextStyle(
                          fontSize: 150, color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(
                          fontSize: 150, color: textColor),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 30.0),
                      child: Text(
                        "Erstellen dein Lebenslauf",
                        style: TextStyle(
                            fontSize: 22, color: textColor),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        Stack(
          children: [
            const SmallScreenShape(),
            Padding(
              padding: const EdgeInsets.symmetric(
                  horizontal: 50.0, vertical: 20.0),
              child: Column(
                children: [
                  const YMargin(20),
                  Row(
                    crossAxisAlignment:
                    CrossAxisAlignment.end,
                    children: const [
                      Text(
                        "2",
                        style: TextStyle(
                            fontSize: 150, color: textColor),
                      ),
                      Text(
                        ".",
                        style: TextStyle(
                            fontSize: 150, color: textColor),
                      ),
                      Padding(
                        padding:
                        EdgeInsets.only(bottom: 30.0),
                        child: Text(
                          "Erstellen dein Lebenslauf",
                          overflow: TextOverflow.clip,
                          softWrap: true,
                          style: TextStyle(
                              fontSize: 22, color: textColor),
                        ),
                      )
                    ],
                  ),
                  const YMargin(10),
                  SvgPicture.asset("assets/svg/task.svg"),
                ],
              ),
            )
          ],
        ),
        SizedBox(
          height: 400,
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                    left: 80.0, top: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "3",
                      style: TextStyle(
                          fontSize: 150, color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(
                          fontSize: 150, color: textColor),
                    ),
                    XMargin(25),
                    Padding(
                      padding: EdgeInsets.only(bottom: 60.0),
                      child: Text(
                        "Mit nur einem Klick\nbewerben",
                        style: TextStyle(
                            fontSize: 22, color: textColor),
                      ),
                    )
                  ],
                ),
              ),
              Positioned(
                  top: 120,
                  left: 0,
                  right: 0,
                  child: SvgPicture.asset(
                      "assets/svg/personal-file.svg"))
            ],
          ),
        )
      ],
    );
  }
}