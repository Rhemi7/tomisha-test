import 'package:flutter/material.dart';

class HeaderShape extends StatelessWidget {
  const HeaderShape({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 200,
      child: CustomPaint(
        painter: ShapePainter(),
      ),
    );
  }
}

class ShapePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final Paint paint = Paint()
      ..color = const Color(0xFFE6FFFA)
      ..strokeWidth = 3.0
      ..style = PaintingStyle.fill;

    var path = Path()
      ..moveTo(0.0, 0.0)
      ..lineTo(0.0, 250)
      ..quadraticBezierTo(size.width / 4, 210.0, size.width / 2, 230)
      ..quadraticBezierTo(size.width - size.width / 4, 245.0, size.width, 210)
      ..lineTo(size.width, 0.0)
      ..close();

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
