import 'package:flutter/material.dart';
import 'package:tomisha_test/constants/color.dart';
import 'package:tomisha_test/helpers/margin.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final bool? showButton;
  const CustomAppBar({super.key, this.showButton = false});
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.vertical(bottom: Radius.circular(20.0)),
        color: Colors.white,
      ),
      child: AppBar(
        backgroundColor: Colors.white,
        elevation: 2.0,
        title: showButton!
            ? Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const Center(
                      child: Text("Jetzt Klicken",
                          style: TextStyle(color: Colors.black))),
                  const XMargin(20),
                  Container(
                    height: 50,
                    width: 250,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: const Color(0xffCBD5E0))),
                    child: const Center(
                        child: Text("Kostenlos Registrieren",
                            style: TextStyle(color: tabTextInactive))),
                  ),
                  const XMargin(20),
                  const Center(
                      child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Login",
                      style: TextStyle(color: tabTextInactive),
                    ),
                  ))
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Center(
                      child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      "Login",
                      style: TextStyle(color: tabTextInactive),
                    ),
                  ))
                ],
              ),
      ),
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(60);
}
