import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:tomisha_test/pages/widget/large_screen_widgets/body_shape.dart';

import '../../../constants/color.dart';
import '../../../helpers/margin.dart';

class LargeTabView extends StatelessWidget {
  const LargeTabView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const YMargin(50),
        const Center(
          child: Text(
            "Drei einfache Schritte\nzu deinem neuen Job",
            style: TextStyle(color: subHeader, fontSize: 30),
          ),
        ),
        const YMargin(60),
        SizedBox(
          height: 900,
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 350.0),
                child: Container(
                  height: 200,
                  decoration: const BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/profile.png"),
                    ),
                  ),
                ),
              ),
             const Positioned(top: 200, child: BodyShape()),
              const Positioned(
                top: 580,
                left: 80,
                child: CircleAvatar(
                  radius: 140,
                  backgroundColor: Color(0xffF7FAFC),
                ),
              ),
              const Positioned(
                // top: 0,
                left: 20,
                child: CircleAvatar(
                  radius: 140,
                  backgroundColor: Color(0xffF7FAFC),
                ),
              ),
              Positioned(
                left: 100,
                child: Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: const [
                        Text(
                          "1",
                          style: TextStyle(fontSize: 150, color: textColor),
                        ),
                        Text(
                          ".",
                          style: TextStyle(fontSize: 150, color: textColor),
                        ),
                        Padding(
                          padding: EdgeInsets.only(bottom: 30.0),
                          child: Text(
                            "Erstellen dein Lebenslauf",
                            style: TextStyle(fontSize: 22, color: textColor),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                  top: 160,
                  left: 220,
                  child: Image.asset(
                    "assets/images/arrow-up.png",
                    scale: 4,
                  )),
              Positioned(
                top: 330,
                left: 500,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "2",
                      style: TextStyle(fontSize: 150, color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(fontSize: 150, color: textColor),
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 30.0),
                      child: Text(
                        "Erstellen dein Lebenslauf",
                        style: TextStyle(fontSize: 22, color: textColor),
                      ),
                    )
                  ],
                ),
              ),
              Positioned(
                  top: 350,
                  left: 150,
                  child: SvgPicture.asset("assets/svg/task.svg")),
              Positioned(
                  top: 500,
                  left: 300,
                  child: Image.asset(
                    "assets/images/arrow-down.png",
                    scale: 4,
                  )),
              Positioned(
                top: 600,
                left: 260,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "3",
                      style: TextStyle(fontSize: 150, color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(fontSize: 150, color: textColor),
                    ),
                    XMargin(10),
                    Padding(
                      padding: EdgeInsets.only(bottom: 35.0),
                      child: Text(
                        "Mit nur einem Klick\nbewerben",
                        style: TextStyle(fontSize: 22, color: textColor),
                      ),
                    ),
                  ],
                ),
              ),
              Positioned(
                  top: 600,
                  left: 600,
                  child: SvgPicture.asset("assets/svg/personal-file.svg"))
            ],
          ),
        ),
      ],
    );
  }
}
