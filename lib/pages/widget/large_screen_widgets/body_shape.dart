import 'package:flutter/material.dart';
class BodyShape extends StatelessWidget {
  const BodyShape({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: CustomPaint(
            painter: ShapeTop(),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 100,
          child: CustomPaint(
            painter: ShapeBottom(),
          ),
        ),
      ],
    );
  }
}

class ShapeBottom extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // final Rect rect = Rect.fromLTRB(-63.47837829589844, 1088, 520 - 63.47837829589844, 1088 + 370);
    //
    // final Gradient gradient = LinearGradient(
    //   colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
    //   stops: [0, 1],
    //   begin: Alignment.topLeft,
    //   end: Alignment.bottomRight,
    // );

    final Paint paint = Paint()
      ..color = Color(0xFFE6FFFA)
      ..strokeWidth = 3.0
      ..style = PaintingStyle.fill;

    var path = Path()
      ..moveTo(0.0, 0.0)
    // ..quadraticBezierTo(size.width/ 3, 50, size.width/3, 50)
    //   ..quadraticBezierTo(size.width - size.width/4, 20, size.width, 30)
      ..lineTo(0.0, 250)
      ..quadraticBezierTo(size.width/ 4, 210.0, size.width / 2, 230)

      ..quadraticBezierTo(size.width - size.width/ 4, 245.0, size.width , 150)

    // ..lineTo(size.width, 200)
      ..lineTo(size.width, 0.0)
      ..close();

    canvas.drawPath(path, paint);


  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class ShapeTop extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // final Rect rect = Rect.fromLTRB(-63.47837829589844, 1088, 520 - 63.47837829589844, 1088 + 370);
    //
    // final Gradient gradient = LinearGradient(
    //   colors: [Color(0xFFE6FFFA), Color(0xFFEBF4FF)],
    //   stops: [0, 1],
    //   begin: Alignment.topLeft,
    //   end: Alignment.bottomRight,
    // );

    final Paint paint = Paint()
      ..color = Color(0xFFE6FFFA)
      ..strokeWidth = 3.0
      ..style = PaintingStyle.fill;

    var path = Path()
      ..moveTo(0.0, 250)
    // ..quadraticBezierTo(size.width/ 3, 50, size.width/3, 50)
    //   ..quadraticBezierTo(size.width - size.width/4, 20, size.width, 30)
      ..lineTo(size.width, 250)
    // ..quadraticBezierTo(size.width/ 4, 210.0, size.width / 2, 230)
    //
    // ..quadraticBezierTo(size.width - size.width/ 4, 245.0, size.width , 210)

    // ..lineTo(size.width, 200)
      ..lineTo(size.width, 80)
      ..quadraticBezierTo(size.width - size.width/4, 5, size.width/2, 40)
      ..quadraticBezierTo(size.width/4, 73, 0, 10);
    // ..close();

    canvas.drawPath(path, paint);


  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
