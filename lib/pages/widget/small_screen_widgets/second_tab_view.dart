import 'package:flutter/material.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/small_screen_shape.dart';
import '../../../constants/color.dart';
import '../../../helpers/margin.dart';


class SecondTabView extends StatelessWidget {
  const SecondTabView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const YMargin(50),
        const Text(
          "Drei einfache Schritte\nzu deinem neuen Mitarbeiter",
          textAlign: TextAlign.center,
          style:
          TextStyle(color: subHeader, fontSize: 22),
        ),
        const YMargin(50),
        SizedBox(
          height: 320,
          width: double.infinity,
          child: Stack(
            children: [
              Container(
                height: 200,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        "assets/images/profile.png"),
                  ),
                ),
              ),
              Positioned(
                top: 150,
                left: 0,
                right: 0,
                child: Row(
                  crossAxisAlignment:
                  CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "1",
                      style: TextStyle(
                          fontSize: 150,
                          color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(
                          fontSize: 150,
                          color: textColor),
                    ),
                    XMargin(20),
                    Padding(
                      padding:
                      EdgeInsets.only(bottom: 30.0),
                      child: Text(
                        "Erstellen dein\nUnternehmensprofil",
                        style: TextStyle(
                            fontSize: 22,
                            color: textColor),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 450,
          child: Stack(
            children: [
              const SmallScreenShape(
                height: 200,
              ),
              Padding(
                padding:
                const EdgeInsets.only(top: 150.0),
                child: Container(
                  height: 250,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              "assets/images/about-me.png"))),
                ),
              ),
              // Image.asset("assets/images/about-me.png",),
              Padding(
                padding: const EdgeInsets.only(
                    left: 50, top: 45, bottom: 45),
                child: Row(
                  crossAxisAlignment:
                  CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "2",
                      style: TextStyle(
                          fontSize: 150,
                          color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(
                          fontSize: 150,
                          color: textColor),
                    ),
                    Padding(
                      padding:
                      EdgeInsets.only(bottom: 30.0),
                      child: Text(
                        "Erstellen dein Jobinserat",
                        style: TextStyle(
                            fontSize: 24,
                            color: textColor),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        // Container(
        //   height: 400,
        //   child: Stack(
        //     children: [
        //       co
        //     ],
        //   ),
        // )
        Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(
                  left: 70.0),
              child: Row(
                crossAxisAlignment:
                CrossAxisAlignment.end,
                children: const [
                  Text(
                    "3",
                    style: TextStyle(
                        fontSize: 150, color: textColor),
                  ),
                  Text(
                    ".",
                    style: TextStyle(
                        fontSize: 150, color: textColor),
                  ),
                  XMargin(22),
                  Padding(
                    padding:
                    EdgeInsets.only(bottom: 60.0),
                    child: Text(
                      "Wähle deinen\nneuen Mitarbeiter aus",
                      style: TextStyle(
                          fontSize: 22, color: textColor),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 280,
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          "assets/images/swipe.png"))),
            ),
            const YMargin(50),
          ],
        )
      ],
    );
  }
}
