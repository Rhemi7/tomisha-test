
import 'dart:ui';

const Color subHeader = Color(0xff4A5568);
const Color textColor = Color(0xff718096);
const Color headerBg = Color(0xffE6FFFA);
const Color headerGBg = Color(0xffEBF4FF);
const Color header = Color(0xff2D3748);
const Color tabBorder = Color(0xffCBD5E0);
const Color tabActive = Color(0xff81E6D9);
const Color tabTextInactive = Color(0xff319795);
const Color buttonGradient = Color(0xff3182CE);
const Color appBarBg = Color(0xffFFFFFF);