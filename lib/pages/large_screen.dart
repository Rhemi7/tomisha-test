import 'package:flutter/material.dart';
import 'package:tomisha_test/app_bar.dart';
import 'package:tomisha_test/helpers/margin.dart';
import 'package:tomisha_test/pages/widget/large_screen_widgets/top_half_large.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/tab_bar_widgets.dart';
import '../constants/color.dart';
import '../helpers/enum.dart';
import 'widget/large_screen_widgets/large_tab_view.dart';

class LargeScreen extends StatefulWidget {
  const LargeScreen({Key? key}) : super(key: key);

  @override
  State<LargeScreen> createState() => _LargeScreenState();
}

class _LargeScreenState extends State<LargeScreen> {
  SelectionEnum selected = SelectionEnum.first;

  ScrollController controller = ScrollController();

  @override
  void initState() {
    //This is used for displaying buttons in the app bar
    controller.addListener(() {
      if (controller.offset > 200) {
        show = true;
      } else {
        show = false;
      }
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  bool show = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showButton: show,
      ),
      body: ListView(
        controller: controller,
        children: [
          const TopHalfLarge(),
          const YMargin(50),
          Center(
            child: Container(
              height: 50,
              width: 450,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  border: Border.all(color: tabBorder)),
              child: Row(
                children: [
                  CornerTab(
                    title: "Arbeitnehmer",
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(14),
                        bottomLeft: Radius.circular(14)),
                    textColor: selected == SelectionEnum.first
                        ? Colors.white
                        : tabTextInactive,
                    bgColor: selected == SelectionEnum.first
                        ? tabActive
                        : Colors.white,
                    onTap: () {
                      setState(() {
                        selected = SelectionEnum.first;
                      });
                    },
                  ),
                  const AppVertDivider(),
                  CenterTab(
                    title: "Arbeitgeber",
                    textColor: selected == SelectionEnum.second
                        ? Colors.white
                        : tabTextInactive,
                    bgColor: selected == SelectionEnum.second
                        ? tabActive
                        : Colors.white,
                    onTap: () {
                      setState(() {
                        selected = SelectionEnum.second;
                      });
                    },
                  ),
                  const AppVertDivider(),
                  CornerTab(
                    title: "Temporärbüro",
                    borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(14),
                        bottomRight: Radius.circular(14)),
                    textColor: selected == SelectionEnum.third
                        ? Colors.white
                        : tabTextInactive,
                    bgColor: selected == SelectionEnum.third
                        ? tabActive
                        : Colors.white,
                    onTap: () {
                      setState(() {
                        selected = SelectionEnum.third;
                      });
                    },
                  ),
                ],
              ),
            ),
          ),
          const LargeTabView()
        ],
      ),
    );
  }
}


