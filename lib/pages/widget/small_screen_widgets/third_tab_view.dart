import 'package:flutter/material.dart';
import 'package:tomisha_test/pages/widget/small_screen_widgets/small_screen_shape.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../../constants/color.dart';
import '../../../helpers/margin.dart';

class ThirdTabView extends StatelessWidget {
  const ThirdTabView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const YMargin(50),
        const Text(
          "Drei einfache Schritte zur \nVermittlung neuer Mitarbeiter",
          textAlign: TextAlign.center,
          style:
          TextStyle(color: subHeader, fontSize: 22),
        ),
        const YMargin(50),
        Container(
          height: 320,
          width: double.infinity,
          child: Stack(
            children: [
              Container(
                height: 200,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(
                        "assets/images/profile.png"),
                  ),
                ),
              ),
              Positioned(
                top: 150,
                left: 0,
                right: 0,
                child: Row(
                  crossAxisAlignment:
                  CrossAxisAlignment.end,
                  children: const [
                    Text(
                      "1",
                      style: TextStyle(
                          fontSize: 150,
                          color: textColor),
                    ),
                    Text(
                      ".",
                      style: TextStyle(
                          fontSize: 150,
                          color: textColor),
                    ),
                    XMargin(20),
                    Padding(
                      padding:
                      EdgeInsets.only(bottom: 30.0),
                      child: Text(
                        "Erstellen dein\nUnternehmensprofil",
                        style: TextStyle(
                            fontSize: 22,
                            color: textColor),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 450,
          child: Stack(
            children: [
              const SmallScreenShape(
                height: 200,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                    horizontal: 50.0, vertical: 20.0),
                child: Column(
                  children: [
                    const YMargin(20),
                    Row(
                      crossAxisAlignment:
                      CrossAxisAlignment.end,
                      children: const [
                        Text(
                          "2",
                          style: TextStyle(
                              fontSize: 150,
                              color: textColor),
                        ),
                        Text(
                          ".",
                          style: TextStyle(
                              fontSize: 150,
                              color: textColor),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              bottom: 30.0),
                          child: Text(
                            "Erhalte Vermittlungs- \nangebot von Arbeitgeber",
                            style: TextStyle(
                                fontSize: 22,
                                color: textColor),
                          ),
                        )
                      ],
                    ),
                    const YMargin(10),
                    SvgPicture.asset(
                        "assets/svg/job-offer.svg"),
                  ],
                ),
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(
              horizontal: 50.0, vertical: 10.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment:
                CrossAxisAlignment.end,
                children: const [
                  Text(
                    "3",
                    style: TextStyle(
                        fontSize: 150, color: textColor),
                  ),
                  Text(
                    ".",
                    style: TextStyle(
                        fontSize: 150, color: textColor),
                  ),
                  Padding(
                    padding:
                    EdgeInsets.only(bottom: 30.0),
                    child: Text(
                      "Vermittlung nach\nProvision oder\nStundenlohn",
                      style: TextStyle(
                          fontSize: 22, color: textColor),
                    ),
                  )
                ],
              ),
              const YMargin(10),
              Container(
                height: 280,
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                            "assets/images/business-deal.png"))),
              ),
            ],
          ),
        )
      ],
    );
  }
}
