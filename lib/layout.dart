import 'package:flutter/material.dart';
import 'package:tomisha_test/pages/large_screen.dart';
import 'package:tomisha_test/pages/small_screen.dart';
import 'helpers/responsiveness.dart';

class Layout extends StatefulWidget {
  const Layout({Key? key}) : super(key: key);

  @override
  State<Layout> createState() => _LayoutState();
}

class _LayoutState extends State<Layout> {
  @override
  Widget build(BuildContext context) {
    return const ResponsiveWidget(
      largeScreen: LargeScreen(),
      smallScreen: SmallScreen(),
    );
  }
}


