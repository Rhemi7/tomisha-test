import 'package:flutter/material.dart';

import '../../../helpers/margin.dart';
import '../app_button.dart';
import 'header_shape.dart';

class TopHalfLarge extends StatelessWidget {
  const TopHalfLarge({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        const HeaderShape(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text(
                      "Deine Job\nwebsite",
                      style: TextStyle(fontSize: 35),
                    ),
                  ),
                  YMargin(20),
                  AppButton()
                ],
              ),
            ),
            Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(top: 15.0),
                  child: Container(
                    height: 200,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                        image: DecorationImage(
                          image: AssetImage("assets/images/handshake.png"),
                        )),
                  ),
                )),
            const Spacer(),
          ],
        )
      ],
    );
  }
}
